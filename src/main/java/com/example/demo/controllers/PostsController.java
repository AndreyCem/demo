package com.example.demo.controllers;

import com.example.demo.models.Post;
import com.example.demo.repository.PostRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RequiredArgsConstructor
@RestController
@RequestMapping ("/post")
class PostController {

    private final PostRepo postRepo;

    /**
     * @param id идентификатор поста в БД
     * @return возвращает {@link Post} из БД
     */
    @GetMapping(path = "/getById", produces = APPLICATION_JSON_VALUE)
    public Post postById(@RequestParam Integer id) {
        return postRepo.findById(id).get();
    }

    @PutMapping(path = "/putPost", produces = APPLICATION_JSON_VALUE)
    public Post addPostToDb(@RequestBody Post post) {
        Post billAfterSave = postRepo.save(post);
        return billAfterSave;
    }
}