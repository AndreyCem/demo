package com.example.demo.controllers;

import com.example.demo.models.Bill;
import com.example.demo.repository.BillRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RequiredArgsConstructor
@RestController
@RequestMapping ("/bills")
class BillingController {

    // TODO КОНВЕРТЕРЫ ДЛЯ ТРАСПОРТА И ЕНТЕТИ РАЗОБРАТЬСЯ
    private final BillRepository billRepository;

    @GetMapping(path = "/getById", produces = APPLICATION_JSON_VALUE)
    public Bill BillById(@RequestParam Integer id) {
        return billRepository.findById(id).get();
    }

    @PutMapping(path = "/putBill", produces = APPLICATION_JSON_VALUE)
    public Bill addBillToDb(@RequestBody Bill bill) {
        Bill billAfterSave = billRepository.save(bill);
        return billAfterSave;
    }
}