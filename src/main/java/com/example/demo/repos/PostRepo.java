package com.example.demo.repos;

import com.example.demo.models.Post;
import org.springframework.data.repository.CrudRepository;

public interface PostRepo extends CrudRepository<Post, Integer> {

}
