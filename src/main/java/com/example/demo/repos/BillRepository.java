package com.example.demo.repos;

import com.example.demo.models.Bill;
import org.springframework.data.repository.CrudRepository;

public interface BillRepository  extends CrudRepository<Bill, Integer>{
}
