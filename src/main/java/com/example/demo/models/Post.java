package com.example.demo.models;

import lombok.*;
import lombok.experimental.Accessors;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@Entity
@Table(name = "post")
public class Post extends BaseEntity  {

    private String text;
    private String tag;

}
